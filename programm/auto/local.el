(TeX-add-style-hook "local"
 (lambda ()
    (LaTeX-add-index-entries
     "\\authorfamily{} \\authorinitialsplain")
    (LaTeX-add-environments
     "scheme")
    (TeX-add-symbols
     '("partNonumber" ["argument"] 1)
     "schemename"
     "mypartname"
     "tableFont"
     "goth"
     "mathscr"
     "etal"
     "PFUudcName"
     "theindex"
     "item"
     "indexname")
    (TeX-run-style-hooks
     "lastpage"
     "breqn"
     "threeparttablex"
     "threeparttable"
     "titletoc"
     "caption"
     "lscape"
     "hhline"
     "array"
     "epigraph"
     "afterpage"
     "url"
     "pdfpages"
     "fancyhdr"
     "geometry")))

